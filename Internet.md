Internet
========

- **objective**: what is exactly *Internet*
- **topic**:  networking
- **related**: [inter-network](inter-network.md) | [IP](IP.md)


Definition
----------

* *Internet* is a global-scale inter-network running the [TCP/IP protocol stack](TCP-IP.md). 
* An inter-network is a set of interconnected networks. Those independent (a maybe heterogenous) networks are connected through [router](router.md)s.
* In fact, the name "Internet" is just the combination of the words "inter" and "net".

*Internet* provides the mechanism to send IP packets from any connected computer on the world to any other ([peer-to-peer](peer-to-peer.md)).

*Internet* is not the same that [web](web.md). The web (WWW) is just one of the thousands of services that may run over *Internet* or any other IP network.


References
----------

* <http://en.wikipedia.org/wiki/Internet>
