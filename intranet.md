intranet
========

- **objective**: what is exactly concepts *intranet*
- **topic**:  networking
- **related**: [inter-network](inter-network.md) | [IP](IP.md)


Definition
----------

An *intranet* is a portion of the [Internet](Internet.md) that is separately administered and has a boundary that can be configured to enforce local 
security policies. [[Coulouris01](bib.md#coulouris01)]
