network security
================

- **objective**: what we mean by *network security* 
- **topic**:  networking
- **related**: [confidentiality](confidentiality.md) | [integrity](integrity.md) | [availability](availability.md)


Definition
----------

Preservation of confidentiality, integrity and availability of network resources. 
Those resources include data, software and hardware both computers and communication devices.

References
----------

* <http://en.wikipedia.org/wiki/Network_security>